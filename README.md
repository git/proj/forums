[//]: # (Copyright 2019 Gentoo Authors)

Gentoo Big Forum Upgrade
========================

## Working with the Forums Repository

The forums repository is set up with multiple independent branches with no
common ancestor. It is also set up to track two different remote git
repositories:

  * The phpBB repository, which will be called "upstream" by convention.
  * The Gentoo Forums repository, which will be called "origin" by convention
    and tradition.

Checking out the repository:
```
git clone git+ssh://git@git.gentoo.org/proj/forums.git
git remote add upstream git@github.com:phpbb/phpbb.git
```

